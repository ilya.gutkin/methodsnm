{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Numerical Integration\n",
    "\n",
    "To form the finite element integrals we need to be able to evaluate integrals of the form $\\int_{\\Omega} \\phi_i \\phi_j dx$, $\\int_{\\Omega} \\nabla \\phi_i \\cdot \\nabla \\phi_j dx$ and $\\int_{\\Omega} f ~ \\phi_i~ dx$. \n",
    "\n",
    "As we will see later, we decompose the domain into a set of elements $\\{T\\} =: \\mathcal{T}_h$ and then decompose the integration problem into the element problems. \n",
    "\n",
    "For now, we will assume that the domain of integration is a single element $T$. \n",
    "\n",
    "This element furthermore can be mapped to a *reference element* of same type (segment, triangle, quadrilateral, tetrahedron, hexahedron) so that the fundamental integration problem to solve is the integration over the reference element.\n",
    "\n",
    "On the reference interval the previously mentioned integrals could be solved analytically.  However, for coefficient functions that are not polynomials, e.g. in the r.h.s. source terms or the diffusivity, this is soon not possible. Therefore, most often **numerical integration** is used in finite element codes."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## 1D Numerical integration\n",
    "\n",
    "As Numerical integration has been considered in the numerics lecture (Numerik II), we will not do a detailed lecture on this topic here. Instead, we will just briefly recap the main ideas and then go to the implement of some standard 1D numerical integration schemes. \n",
    "\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "\n",
    "#### The integration problem:\n",
    "$$\n",
    "I(f) := \\int_{0}^{1} f(x) dx \\approx Q(f) := \\sum_{i=1}^{n} w_i f(x_i) \\tag{Q}\n",
    "$$\n",
    "\n",
    "Let's do a brief recap of numerical integration based on a few repition questions (if needed we will do a more detailed lecture/session on this):\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Interpolation quadrature:\n",
    "\n",
    "* What is the idea of interpolation quadrature?\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "\n",
    "#### Interpolation Exactness:\n",
    "\n",
    "* What is the exactness degree of a quadrature scheme?\n",
    "* How can it be used constructively to come up with a quadrature scheme of a certain exactness degree?\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "\n",
    "#### Gaussian quadrature:\n",
    "\n",
    "* What is the idea of Gaussian quadrature?\n",
    "* Which accuracy can be achieved with Gaussian quadrature?\n",
    "* How are Gaussian quadrature points and weights computed? How are they related to orthogonal polynomials?\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "\n",
    "### Implementation: $\\leadsto$ [intrule.py](intrule.py), [intrule_1d.py](intrule_1d.py)\n",
    "\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Tasks:"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Task `Int1D`-1:\n",
    "\n",
    "Complete the implementation of the Newton-Cotes Rules of arbitrary order $n$ (see [intrule_1d.py](intrule_1d.py)) and make sure that the unit test `test_newtoncotes` in `tests/test_intrule_1d.py` passes."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Task `Int1D`-2:\n",
    "\n",
    "Implement the Gaussian-Legendre quadrature rule of order $n$ and make sure that the corresponding unit tests `tests/test_intrule_1d.py` passes. Add your own unit tests, e.g. to check for positivity of the weights.\n",
    "\n",
    "To compute the nodes and weights you can choose different approaches:\n",
    "    * formulate a nonlinear problem based on the exactness property\n",
    "    * compute the zeros of the corresponding orthogonal polynomials, e.g. by a combination of a bisection and Newton's method\n",
    "\n",
    "To test your implementation you can compare to the numpy-implementation of the Gauss-Legendre quadrature rule which is wrapped into the `NP_GaussLegendreRule` in [intrule_1d.py](intrule_1d.py)."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Task `Int1D`-3:\n",
    "\n",
    "For integration tasks of the form $\\int_{-1}^{1} (1-x)^\\alpha (1+x)^\\beta f(x) dx$, i.e. the quadrature problem with the positive weight function $w(x) = (1-x)^\\alpha (1+x)^\\beta$, the Gauss-Rules are called Gauss-Jacobi rules and the corresponding orthogonal polynomials are the Jacobi polynomials that we saw before in [fe.ipynb](fe.ipynb).\n",
    "\n",
    "Now, implement the Gauss-Jacobi quadrature rule of order $n$ and make sure that the corresponding unit test (for $\\alpha=\\beta=0$) in `tests/test_intrule_1d.py` passes. Add your own unit tests (for $(\\alpha,\\beta)\\neq (0,0)$). Again, you may compare to existing implementations from scipy which is wrapped into the `SP_GaussJacobiRule` in [intrule_1d.py](intrule_1d.py).\n",
    "\n",
    "For the implementation you can use the same approaches as for the Gauss-Legendre rule above.\n",
    "\n",
    "These integration rules will be important for arbitrary order integration rules on triangles later."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## 2D Numerical integration\n",
    "\n",
    "For low and moderate orders of the integration problem we can proceed as for the 1D case while for higher orders we typically want to resort to tensor product rules at some point. These are easily set up for quadrilaterals and hexahedrons but need some tricks to be applied on triangles and tetrahedrons. \n",
    "\n",
    "In the context of this course we will concencrate on triangles in 2D and use the following triangle as the reference triangle:\n",
    "\n",
    "$\\hat{T} = \\operatorname{conv}\\{(0,0),(1,0),(0,1)\\} = \\{ (x,y) \\in \\mathbb{R}^2 ~|~ x\\geq 0, y\\geq 0, x+y\\leq 1 \\}$\n",
    "\n",
    "Implementation: [intrule_2d.py](intrule_2d.py)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Low order integration rules\n",
    "\n",
    "Based on exactness demands and counting arguments (dimension of the space of polynomials) and a selection of points, weights can be computed."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Task `Int2D`-1:\n",
    "\n",
    "Implement the a vertex integration rules for triangles, i.e. find the weights $w_i$ such that the integration rule\n",
    "$Q(f) = \\sum_{i=1}^{3} w_i f(x_i,y_i)$ is exact for all polynomials of degree $\\leq 1$ where \n",
    "$(x_0,y_0) = (0,0)$, $(x_1,y_1) = (1,0)$ and, $(x_2,y_2) = (0,1)$.\n",
    "\n",
    "Here, you may want to compute the weights on paper and simply hack them into the code or use a generic approach to compute the weights (which is more involved, but may help for the proceeding tasks).\n",
    "\n",
    "Check the exactness degree and implement a test for the integration rule in a new test file `tests/test_intrule_2d.py`.\n",
    "\n",
    "#### Task `Int2D`-2:\n",
    "Combining the edge midpoints and the vertices, we have $6$ points in total. Find the weights $w_i$ such that the integration rule is exact for all polynomials of degree $\\leq 2$.\n",
    "Implement the rule and test it.\n",
    "\n",
    "#### Task `Int2D`-2:\n",
    "For a Gauss-Legendre rule with 3 nodes we have $6$ degrees of freedom (3 nodes and 3 weights). Therefore, we can expect to be able to integrate polynomials of degree $\\leq 2$ exactly. \n",
    "\n",
    "Finding the nodes and weights is a nonlinear problem and will most certainly require a numerical approach. Write a proper nonlinear solver for the problem and implement the integration rule. Afterwards test it."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## High order integration rules and the Duffy transformation"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.11.5"
  },
  "orig_nbformat": 4
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
